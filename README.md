# BSPWM-rice
Drake's Parabola GNU/Linux rice running BSPWM, based on Luke Smith's Voidrice.

# Installation
Use Luke's LARBS script with the -r flag and the url of this repo, selecting the "custom" option when prompted, to install my dotfiles. Be sure to install BSPWM first.

# NOTE
These are my personal dotfiles. Use at your own risk.
